Demo for AspectJ Parallel ClassLoader Issue
=============================================

Prepare
-------

1. Have AspectJ installed in your local Maven repository
1. Update AspectJ version in the pom.xml file if necessary

Build and Run
-------------
```
./mvnw clean package
```

Expected Results
----------------

During the build, the following output should be emitted:

```
Calling Hello from pool-1-thread-1
Calling Hello from pool-1-thread-2
Hello, World!
Hello, World!
```

Actual (if the issue reproduces)
----------------------------------
```
Hello, World!
Hello, World!
```

Because this issue is a race condition, the issue does not reproduce 100% of the
time, but you should see it after a few runs.

Running from Command Line
-------------------------

```
cd target/classes
java -javaagent:/path/to/aspectjweaver.jar com.example.SampleClass
```

