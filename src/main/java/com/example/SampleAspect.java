package com.example;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SampleAspect {
    @Pointcut("execution(public String com.example.MyClass.hello())")
    public void myClassHello() {
    }

    @Around("myClassHello()")
    public Object aroundMyClassHello(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("Calling Hello from " + Thread.currentThread().getName());
        return pjp.proceed();
    }
}
