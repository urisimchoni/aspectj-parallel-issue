package com.example;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SampleClass {
    public static void main(String[] args) throws Exception {
        var executor = Executors.newFixedThreadPool(2);
        var classToLoad = "com.example.MyClass";
        var cl = new MyClassLoader(SampleClass.class.getClassLoader(), classToLoad);
        for (int i = 0; i < 2; i++) {
            executor.submit(() -> {
                try {
                    Class<?> myClass = Class.forName(classToLoad, true, cl);
                    Helloer helloer = (Helloer) myClass.getConstructor(new Class<?>[] {}).newInstance();
                    System.out.println("Hello, " + helloer.hello());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        }
        executor.shutdown();
        executor.awaitTermination(60, TimeUnit.SECONDS);
    }
}
